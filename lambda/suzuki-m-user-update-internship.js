var AWS = require("aws-sdk");
const crypto = require("crypto");
var dynamo = new AWS.DynamoDB.DocumentClient();
var tableName = "user";

exports.handler = (event, context, callback) => {
    var response = {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Origin" : "*"
        },
        body: JSON.stringify({"message" : ""})
    };

    var body = JSON.parse(event.body);

    if (!body.userId || !body.password || !body.nickname || !body.age) {
        console.log('不足');
        response.statusCode = 400;
        response.body = JSON.stringify({
            'message': 'Register Error',
            'detail': 'パラメータ不足です'
        });
        callback(null, response);
        return;
    }

    const sha512 = crypto.createHash('sha512');
    var param = {
        'TableName': tableName,
        'Item': {
            'userId': body.userId,
            'password': sha512.update(body.password).digest('hex'),
            'nickname' : body.nickname,
            'age': body.age
        },
    };

    //dynamo.put()を用いてデータの更新
    dynamo.put(param, function(err, data){
        if(err){
            response.statusCode = 500;
            response.body = JSON.stringify({
                "message": "DynamoDB Error",
                "detail": err
            });
            callback(null, response);
            return;
        }else{
            //TODO: 更新に成功した場合の処理を記述
            delete param.Item.password;
            param = param.Item;
            response.statusCode = 200;
            response.body = JSON.stringify({
                param
            });
            callback(null, response);
            return;
        }
    });
};
