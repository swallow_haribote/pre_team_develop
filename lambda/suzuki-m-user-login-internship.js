var AWS = require("aws-sdk");
const crypto = require("crypto");
var dynamo = new AWS.DynamoDB.DocumentClient();
var tableName = "user";

exports.handler = (event, context, callback) => {
    var response = {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Origin" : "*"
        },
        body: JSON.stringify({"message" : ""})
    };

    var body = JSON.parse(event.body);

    var userId = body.userId;
    // passwordのhash
    const sha512 = crypto.createHash('sha512');
    var password = sha512.update(body.password).digest('hex');
    //TODO: query()に渡すparamを宣言
    var param = {
        "TableName" : tableName,
        //キー、インデックスによる検索の定義
        "KeyConditionExpression" :
            "userId = :userId",
        //プライマリーキー以外の属性でのフィルタ
        "FilterExpression":
            "#password = :password",
        //属性名のプレースホルダの定義
        "ExpressionAttributeNames" : {
            "#password": "password"
        },
        //検索値のプレースホルダの定義
        "ExpressionAttributeValues" : {
            ":userId": userId,
            ":password": password
        }
    };

    //dynamo.query()を用いてuserIdとpasswordが一致するデータの検索
    dynamo.query(param, function(err, data){
        //userの取得に失敗
        if(err){
            response.statusCode = 500;
            response.body = JSON.stringify({
                "message": "DynamoDB Error",
                "detail": err
            });
            callback(null, response);
            return;
        }
        //TODO: 該当するデータが見つからない場合の処理を記述(ヒント：data.Itemsの中身が空)
        if (data.Items.length === 0) {
            response.statusCode = 404;
            response.body = JSON.stringify({
                "message": "user data Not Found",
            });
            callback(null, response);
            return;
        }

        //TODO: 認証が成功した場合のレスポンスボディとコールバックを記述
        response.statusCode = 200;
        response.body = JSON.stringify({
            "token": "mti2019",
        });
        callback(null, response);
        return;

    });
};
