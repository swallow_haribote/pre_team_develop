var AWS = require("aws-sdk");
var dynamo = new AWS.DynamoDB.DocumentClient();
var tableName = "user";

exports.handler = (event, context, callback) => {
    //レスポンスの雛形
    var response = {
        statusCode : 200,
        headers: {
            "Access-Control-Allow-Origin" : "*"
        },
        body: JSON.stringify({"message" : ""})
    };

    // if (!event.queryStringParameters.userId) {
    //     console.log('不足');
    //     response.statusCode = 400;
    //     response.body = JSON.stringify({
    //         'message': 'Register Error',
    //         'detail': 'パラメータ不足です'
    //     });
    //     callback(null, response);
    //     return;
    // }

    var userId = event.queryStringParameters.userId;   //見たいユーザのuserId


    //TODO: 取得対象のテーブル名と検索に使うキーをparamに宣言
    var param = {
        'TableName': tableName,
        'Key': {
            'userId': userId
        }
    };

    //dynamo.get()でDBからデータを取得
    dynamo.get(param, function(err, data){
        if(err){
            response.statusCode = 500;
            response.body = JSON.stringify({
                "message": "DynamoDB Error",
                "detail": err
            });
            callback(null, response);
            return;
        }

        //TODO: 条件に該当するデータがあればパスワードを隠蔽をする処理を記述
        delete data.Item.password;

        //TODO: レスポンスボディの設定とコールバックを記述
        data = data.Item;
        response.statusCode = 200;
        response.body = JSON.stringify({
            data
        });
        callback(null, response);

    });
};
