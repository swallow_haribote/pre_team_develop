var vm = new Vue({
    el: "#app", // Vue.jsを使うタグのIDを指定
    data: {
    // Vue.jsで使う変数はここに記述する
        mode: 'login',
        submitText: 'ログイン',
        toggleText: '新規登録',
        loginError: false,
        user: {
            id: '',
            password: '',
            nickname: '',
            age: 0,
        }
    },
    computed: {
    // 計算した結果を変数として利用したいときはここに記述する
    },
    created: function() {
    // Vue.jsの読み込みが完了したときに実行する処理はここに記述する
    },
    methods: {
    // Vue.jsで使う関数はここで記述する
        changeForm(toggleText) {
            if (toggleText === '新規登録') {
                vm.submitText = '新規登録';
                vm.toggleText = 'ログイン';
                vm.mode = 'signup';
            } else {
                vm.submitText = 'ログイン';
                vm.toggleText = '新規登録';
                vm.mode = 'login';
            } 
        },
        submit() {
            if (vm.mode === 'login') {
                // APIにPOSTリクエストを送る
                fetch(url + "/user/login", {
                    method: "POST",
                    body: JSON.stringify({
                        userId: vm.user.id,
                        password: vm.user.password
                    })
                })
                    .then(function(response) {
                        if (response.status == 200) {
                            return response.json();
                        }
                        // 200番以外のレスポンスはエラーを投げる
                        return response.json().then(function(json) {
                            if (response.status === 404) {
                                vm.loginError = true;
                            } else {
                                throw new Error(json.message);
                            }
                        });
                    })
                    .then(function(json) {
                    // レスポンスが200番で返ってきたときの処理はここに記述する
                        localStorage.setItem('token', json.token);
                        localStorage.setItem('userId', vm.user.id);
                        location.href = "./index.html";
                    })
                    .catch(function(err) {
                    // レスポンスがエラーで返ってきたときの処理はここに記述する
                    });
            } else {
                // APIにPOSTリクエストを送る
                fetch(url + "/user/signup", {
                    method: "POST",
                    body: JSON.stringify({
                        userId: vm.user.id,
                        password: vm.user.password,
                        nickname: vm.user.nickname,
                        age: Number(vm.user.age)
                    })
                })
                    .then(function(response) {
                        if (response.status == 200) {
                            return response.json();
                        }
                        // 200番以外のレスポンスはエラーを投げる
                        return response.json().then(function(json) {
                            throw new Error(json.message);
                        });
                    })
                    .then(function(json) {
                    // レスポンスが200番で返ってきたときの処理はここに記述する
                        localStorage.setItem('token', 'mti2019');
                        localStorage.setItem('userId', vm.user.id);
                        location.href = "./index.html";
                    })
                    .catch(function(err) {
                    // レスポンスがエラーで返ってきたときの処理はここに記述する
                    });
            }
        },
    },
});
