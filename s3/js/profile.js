var vm = new Vue({
    el: "#app", // Vue.jsを使うタグのIDを指定
    data: {
    // Vue.jsで使う変数はここに記述する
        user: {
            id: '',
            password: '',
            nickname: '',
            age: 0,
        },
        updateComplete: false,
        outData: {}
    },
    computed: {
    // 計算した結果を変数として利用したいときはここに記述する
    },
    created: function() {
    // Vue.jsの読み込みが完了したときに実行する処理はここに記述する
        if (! localStorage.getItem('token')) {
            location.href = "./login.html";
        }
        // APIにPOSTリクエストを送る
        fetch(url + "/user/get" +
            "?userId="+localStorage.getItem("userId"), {
            method: "GET"
        })
            .then(function(response) {
                if (response.status == 200) {
                    return response.json();
                }
                // 200番以外のレスポンスはエラーを投げる
                return response.json().then(function(json) {
                    throw new Error(json.message);
                });
            })
            .then(function(json) {
            // レスポンスが200番で返ってきたときの処理はここに記述する
                vm.user.id = json.data.userId;
                vm.user.nickname = json.data.nickname;
                vm.user.age = json.data.age;
            })
            .catch(function(err) {
            // レスポンスがエラーで返ってきたときの処理はここに記述する
            });

    },
    methods: {
    // Vue.jsで使う関数はここで記述する
        unsubscribe(){
            // APIにPOSTリクエストを送る
            fetch(url + "/user", {
                method: "DELETE",
                // headers: new Headers({
                //     "Authorization": localStorage.getItem ('token')
                // }),
                body: JSON.stringify({
                    userId: localStorage.getItem('userId')
                })
            })
                .then(function(response) {
                    if (response.status == 200) {
                        return response.json();
                    }
                    // 200番以外のレスポンスはエラーを投げる
                    return response.json().then(function(json) {
                        throw new Error(json.message);
                    });
                })
                .then(function(json) {
                // レスポンスが200番で返ってきたときの処理はここに記述する
                    localStorage.removeItem('userId');
                    localStorage.removeItem('token');
                    location.href = "./login.html";
                })
                .catch(function(err) {
                // レスポンスがエラーで返ってきたときの処理はここに記述する
                });
        },
        submit() {
            // APIにPOSTリクエストを送る
            fetch(url + "/user", {
                method: "PUT",
                // headers: new Headers({
                //     "Authorization": localStorage.getItem ('token')
                // }),
                body: JSON.stringify({
                    userId: vm.user.id,
                    password: vm.user.password,
                    nickname: vm.user.nickname,
                    age: Number(vm.user.age)
                })
            })
                .then(function(response) {
                    if (response.status == 200) {
                        return response.json();
                    }
                    // 200番以外のレスポンスはエラーを投げる
                    return response.json().then(function(json) {
                        throw new Error(json.message);
                    });
                })
                .then(function(json) {
                // レスポンスが200番で返ってきたときの処理はここに記述する
                    console.log(json);
                    vm.outData = json;
                    vm.updateComplete = true;
                })
                .catch(function(err) {
                // レスポンスがエラーで返ってきたときの処理はここに記述する
                });
        },
        closeMessage() {
            vm.updateComplete = false;
        }
    },
});
