Vue.component("common-menu", {
    props: ['current'],
    template: `
        <div class="ui secondary pointing green inverted massive menu">
            <a class="item" href="./index.html" v-bind:class="{activeMenu: current==='Index'}">Index</a>
            <a class="item" href="./users.html" v-bind:class="{activeMenu: current==='Users'}">Users</a>
            <a class="item" href="./profile.html" v-bind:class="{activeMenu: current==='Profile'}">Profile</a>
            <div class="right menu">
                    <button class="item" v-on:click="logOut()">Logout</button>
            </div>
        </div>
    `,
    methods: {
        logOut() {
            localStorage.removeItem('userId');
            localStorage.removeItem('token');
            location.href = "./login.html";
        }
    },
});
